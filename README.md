Install link: https://gitlab.com/Wilco7302/compendium-image-mapper/-/raw/master/dist/module.json

To use: Navigate to the compendium tab, and click the "Compendium Mapper" button

# no longer supports versions under 10.x
if you are using a version lower than this please download this package and install manually:
https://gitlab.com/Wilco7302/compendium-image-mapper/-/raw/master/package/imagemapper-v0.8.12.zip

# Compendium Image Mapper

Did you buy a bunch of tokens / images, and are now dreading having to go through all the compendiums to manually select the correct image? Compendium Image Mapper is here to help!
This simple tool to lets you quickly assign a collection of images to your compendium entries.

What it does: Allow you to select a folder containing images, and then automatically search for compendium entries that best match your images. At which point you can automatically or manually apply them.

features:
- automatically map images if there is a high % match between file/entry name
- set % match threshold for automatic/manual mapping
- (optional) manually resolve ties in matches
- (optional) manually confirm if an image should be assigned
- skip entries that already have an image
- set token or portrait image
- undo last mapping

![enter image description here](https://i.imgur.com/NX6mxuL.png)

![enter image description here](https://i.imgur.com/ZalGIb8.gif)

# Thanks to
@zarmstrong - persistent settings & fix for v10
@kellynabours - update compendium fix for 0.8.x

## bugs/improvements

For any bugs/improvements please contact me on the foundry discord under Wilco#7302

Next improvement: Implement automatic re-mapping, so that when the game system updates it will (optionally) automatically apply the images again

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Compendium Image Mapper - a module for Foundry VTT -</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/Wilco7302?tab=repositories" property="cc:attributionName" rel="cc:attributionURL">Wilco S</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](https://foundryvtt.com/article/license/).
